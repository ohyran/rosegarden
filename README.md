# Rosegarden

A Plasma theme for Harriet aka Harald Sitter for Christmas 2019. A set of themes, one dark and one light for Harriets Henley Regatta doo's

## Dark and Light Version

Both are available and contain icon themes (based off of Papirus), GTK theme (created through Oomox), Colour scheme etc.

## Issues

The icon theme is playing merry hell for me in Inkscape as the dark icon theme still display the light variants panel colours and I have yet to get that to work properly.

## Installation

You can put the icon themes in ~/.local/share/icons/, the colour schemes in ~/.local/share/color-schemes
